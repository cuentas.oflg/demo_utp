<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumno".
 *
 * @property int $alumno_id
 * @property string $alumno_nombre
 * @property string $alumno_apellido
 * @property string $alumno_dni
 */
class Alumno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alumno_nombre', 'alumno_apellido', 'alumno_dni'], 'required'],
            [['alumno_nombre', 'alumno_apellido'], 'string', 'max' => 100],
            [['alumno_dni'], 'string', 'max' => 15],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'alumno_id' => Yii::t('app', 'Alumno ID'),
            'alumno_nombre' => Yii::t('app', 'Alumno Nombre'),
            'alumno_apellido' => Yii::t('app', 'Alumno Apellido'),
            'alumno_dni' => Yii::t('app', 'Alumno Dni'),
        ];
    }
}
