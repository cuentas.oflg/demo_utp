<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "curso".
 *
 * @property int $curso_id
 * @property string $curso_nombre
 */
class Curso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'curso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['curso_nombre'], 'required'],
            [['curso_nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'curso_id' => Yii::t('app', 'Curso ID'),
            'curso_nombre' => Yii::t('app', 'Curso Nombre'),
        ];
    }
}
