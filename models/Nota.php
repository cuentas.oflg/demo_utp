<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nota".
 *
 * @property int $nota_id
 * @property int $alumno_id
 * @property int $curso_id
 * @property int $nota
 * @property string $estado
 */
class Nota extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nota';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alumno_id', 'curso_id', 'nota', 'estado'], 'required'],
            [['alumno_id', 'curso_id', 'nota'], 'integer'],
            [['estado'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nota_id' => Yii::t('app', 'Nota ID'),
            'alumno_id' => Yii::t('app', 'Alumno ID'),
            'curso_id' => Yii::t('app', 'Curso ID'),
            'nota' => Yii::t('app', 'Nota'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }
}
