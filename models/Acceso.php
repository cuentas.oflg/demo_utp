<?php

namespace app\models;

use Firebase\JWT\JWT;

use Yii;

/**
 * This is the model class for table "acceso".
 *
 * @property int $acceso_id
 * @property string $usuario
 * @property string $clave
 * @property int $rol
 */
class Acceso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acceso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'clave', 'rol'], 'required'],
            [['rol'], 'integer'],
            [['usuario'], 'string', 'max' => 15],
            [['clave'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'acceso_id' => Yii::t('app', 'Acceso ID'),
            'usuario' => Yii::t('app', 'Usuario'),
            'clave' => Yii::t('app', 'Clave'),
            'rol' => Yii::t('app', 'Rol'),
        ];
    }

    public static function crearJWT($acceso_id, $usuario, $rol)
    {
        try {
            $tiempo = time();
            $llave = $usuario;
            $cuerpo = array(
                'iat' => $tiempo,
                'exp' => $tiempo + 60 * 30,
                'data' => [
                    'id' => $acceso_id,
                    'usuario' => $usuario,
                    'rol' => $rol
                ]
            );
            $jwt = JWT::encode($cuerpo, $llave);
            return $jwt;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    public static function validarJWT($jwt, $llave)
    {
        try {
            $jwt_decode = JWT::decode($jwt, $llave, array('HS256'));
            return array(
                "estado" => 1, "mensaje" => "OK", "resultado" => (array)$jwt_decode->data
            );
        } catch (\Firebase\JWT\SignatureInvalidException $e) {
            return array(
                "estado" => 0, "mensaje" => "JWT INVALIDO"
            );
        } catch (\Firebase\JWT\ExpiredException $e) {
            return array(
                "estado" => 0, "mensaje" => "JWT EXPIRADO"
            );
        }
    }
}
