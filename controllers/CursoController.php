<?php

namespace app\controllers;

use Yii;
use app\models\Curso;
use app\models\Acceso;
use yii\web\Controller;
use yii\filters\VerbFilter;

class CursoController extends Controller
{

    public function behaviors()
    {
        Yii::$app->request->enableCsrfValidation = false;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $post = Yii::$app->request->post();
        $resultado = array();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $model = Curso::find()->all();
            foreach ($model as $llave => $data) {
                $resultado[$llave] = array(
                    "curso_id" => $data->curso_id,
                    "curso_nombre" => $data->curso_nombre
                );
            }
            $return = array(
                "estado" => 1, "mensaje" => "OK", "resultado" => $resultado
            );
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO", "resultado" => $resultado
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionObtener()
    {
        $post = Yii::$app->request->post();
        $resultado = array();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $curso_id = $post['curso_id'];
            $model = Curso::findOne($curso_id);

            if ($model) {
                $resultado = array(
                    "curso_id" => $model->curso_id,
                    "curso_nombre" => $model->curso_nombre
                );
            }
            $return = array(
                "estado" => 1, "mensaje" => "OK", "resultado" => $resultado
            );
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO", "resultado" => $resultado
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionCrear()
    {
        $post = Yii::$app->request->post();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $curso_nombre = $post['curso_nombre'];
            $model = new Curso();
            $model->curso_nombre = $curso_nombre;
            if ($model->save()) {
                $return = array(
                    "estado" => 1, "mensaje" => "OK"
                );
            } else {
                $return = array(
                    "estado" => 1, "mensaje" => "NO SE PUDO CREAR"
                );
            }
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO"
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionModificar()
    {
        $post = Yii::$app->request->post();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $curso_id = $post['curso_id'];
            $curso_nombre = $post['curso_nombre'];

            $model = Curso::findOne($curso_id);
            if ($model) {
                $model->curso_id = $curso_id;
                $model->curso_nombre = $curso_nombre;
                if ($model->save()) {
                    $return = array(
                        "estado" => 1, "mensaje" => "OK"
                    );
                } else {
                    $return = array(
                        "estado" => 0, "mensaje" => "NO SE PUDO MODIFICAR"
                    );
                }
            } else {
                $return = array(
                    "estado" => 0, "mensaje" => "NO SE OBTUVO RESULTADO"
                );
            }
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO"
            );
        } finally {
            echo json_encode($return);
        }
    }
}
