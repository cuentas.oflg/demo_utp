<?php

namespace app\controllers;

use Yii;
use app\models\Alumno;
use app\models\Acceso;
use yii\web\Controller;
use yii\filters\VerbFilter;

class AlumnoController extends Controller
{

    public function behaviors()
    {
        Yii::$app->request->enableCsrfValidation = false;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $post = Yii::$app->request->post();
        $resultado = array();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $model = Alumno::find()->all();
            foreach ($model as $llave => $data) {
                $resultado[$llave] = array(
                    "alumno_id" => $data->alumno_id,
                    "alumno_nombre" => $data->alumno_nombre,
                    "alumno_apellido" => $data->alumno_apellido,
                    "alumno_dni" => $data->alumno_dni
                );
            }
            $return = array(
                "estado" => 1, "mensaje" => "OK", "resultado" => $resultado
            );
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO", "resultado" => $resultado
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionObtener()
    {
        $post = Yii::$app->request->post();
        $resultado = array();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $alumno_dni = $post['alumno_dni'];
            $model = Alumno::find()
                ->where(['alumno_dni' => $alumno_dni])->one();

            if ($model) {
                $resultado = array(
                    "alumno_id" => $model->alumno_id,
                    "alumno_nombre" => $model->alumno_nombre,
                    "alumno_apellido" => $model->alumno_apellido,
                    "alumno_dni" => $model->alumno_dni
                );
            }
            $return = array(
                "estado" => 1, "mensaje" => "OK", "resultado" => $resultado
            );
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO", "resultado" => $resultado
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionCrear()
    {
        $post = Yii::$app->request->post();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $alumno_nombre = $post['alumno_nombre'];
            $alumno_apellido = $post['alumno_apellido'];
            $alumno_dni = $post['alumno_dni'];

            $modelAlumno = Alumno::find()
                ->where(['alumno_dni' => $alumno_dni])->one();

            if ($modelAlumno) {
                $return = array(
                    "estado" => 0, "mensaje" => "DNI existente"
                );
            } else {
                $model = new Alumno();
                $model->alumno_nombre = $alumno_nombre;
                $model->alumno_apellido = $alumno_apellido;
                $model->alumno_dni = $alumno_dni;

                if ($model->save()) {
                    $return = array(
                        "estado" => 1, "mensaje" => "OK"
                    );
                } else {
                    $return = array(
                        "estado" => 1, "mensaje" => "NO SE PUDO CREAR"
                    );
                }
            }
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO"
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionModificar()
    {
        $post = Yii::$app->request->post();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $alumno_nombre = $post['alumno_nombre'];
            $alumno_apellido = $post['alumno_apellido'];
            $alumno_dni = $post['alumno_dni'];

            $model = Alumno::find()
                ->where(['alumno_dni' => $alumno_dni])->one();

            if ($model) {
                $model->alumno_nombre = $alumno_nombre;
                $model->alumno_apellido = $alumno_apellido;
                if ($model->save()) {
                    $return = array(
                        "estado" => 1, "mensaje" => "OK"
                    );
                } else {
                    $return = array(
                        "estado" => 0, "mensaje" => "NO SE PUDO MODIFICAR"
                    );
                }
            } else {
                $return = array(
                    "estado" => 0, "mensaje" => "NO SE OBTUVO RESULTADO"
                );
            }
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO"
            );
        } finally {
            echo json_encode($return);
        }
    }
}
