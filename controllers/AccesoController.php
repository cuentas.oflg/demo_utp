<?php

namespace app\controllers;

use Yii;
use app\models\Acceso;
use app\models\AccesoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class AccesoController extends Controller
{

    public function behaviors()
    {
        Yii::$app->request->enableCsrfValidation = false;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $resultado = array();
        $return = array();
        try {
            $post = Yii::$app->request->post();
            $usuario = $post['usuario'];
            $clave = $post['clave'];
            $model = Acceso::find()
                ->where(['=', 'usuario', $usuario])
                ->andWhere(['=', 'clave', $clave])->one();
            if ($model) {
                $resultado = Acceso::crearJWT($model->acceso_id, $model->usuario, $model->rol);
                $return = array(
                    "estado" => 1, "mensaje" => "OK", "resultado" => $resultado
                );
            } else {
                $return = array(
                    "estado" => 0, "mensaje" => "Credenciales Incorrectas", "resultado" => $resultado
                );
            }
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO", "resultado" => $resultado
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionValidarJwt()
    {
        $return = array();
        try {
            $post = Yii::$app->request->post();
            $usuario = $post['usuario'];
            $jwt = $post['jwt'];
            $return = Acceso::validarJWT($jwt, $usuario);
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO"
            );
        } finally {
            echo json_encode($return);
        }
    }
}
