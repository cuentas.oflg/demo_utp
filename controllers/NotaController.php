<?php

namespace app\controllers;

use Yii;
use app\models\Nota;
use app\models\Curso;
use app\models\Alumno;
use app\models\Acceso;
use yii\web\Controller;
use yii\filters\VerbFilter;

class NotaController extends Controller
{

    public function behaviors()
    {
        Yii::$app->request->enableCsrfValidation = false;
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionNotaAlumno()
    {
        $post = Yii::$app->request->post();
        $resultado = array();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            if ($usuario != $post['alumno_dni']) {
                echo json_encode(array(
                    "estado" => 0, "mensaje" => "ACCESO DENEGADO"
                ));
                exit();
            }
        }

        try {
            $alumno_dni = $post['alumno_dni'];
            $alumno = Alumno::find()->where(['alumno_dni' => $alumno_dni])->one();
            if ($alumno) {
                $notaModel = Nota::find()
                    ->select(['nota.curso_id', 'nota.nota', 'nota.estado'])
                    ->innerJoin('curso', 'curso.curso_id = nota.curso_id')
                    ->where(['alumno_id' => $alumno->alumno_id])
                    ->orderBy('curso.curso_nombre ASC')
                    ->all();
                foreach ($notaModel as $llave => $data) {
                    $curso = Curso::findOne($data->curso_id);
                    $resultado[$llave] = array(
                        "curso_id" => $data->curso_id,
                        "curso_nombre" => $curso->curso_nombre,
                        "nota" => $data->nota,
                        "estado" => $data->estado
                    );
                }
                $return = array(
                    "estado" => 1, "mensaje" => "OK", "resultado" => array(
                        "Alumno" => $alumno->alumno_nombre . ' ' . $alumno->alumno_apellido,
                        "Notas" => $resultado
                    )
                );
            } else {
                $return = array(
                    "estado" => 0, "mensaje" => "NO EXISTE EL ALUMNO INGRESADO"
                );
            }
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO", "resultado" => $resultado
            );
        } finally {
            echo json_encode($return);
        }
    }

    public function actionRegistrarNota()
    {
        $post = Yii::$app->request->post();
        $return = array();

        $usuario = $post['usuario'];
        $jwt = $post['jwt'];
        $validarJWT = Acceso::validarJWT($jwt, $usuario);
        if ($validarJWT['estado'] != 1) {
            echo json_encode($validarJWT);
            exit();
        } else if ($validarJWT['resultado']['rol'] != 1) {
            echo json_encode(array(
                "estado" => 0, "mensaje" => "ACCESO DENEGADO"
            ));
            exit();
        }

        try {
            $curso_id = $post['curso_id'];
            $alumno_dni = $post['alumno_dni'];
            $nota = $post['nota'];

            $alumno = Alumno::find()->where(['alumno_dni' => $alumno_dni])->one();
            if ($alumno) {
                $curso = Curso::findOne($curso_id);
                if ($curso) {
                    $notaModel = Nota::find()
                        ->where(['curso_id' => $curso_id])
                        ->andWhere(['alumno_id' => $alumno->alumno_id])->one();
                    if ($notaModel) {
                        $return = array(
                            "estado" => 0, "mensaje" => "Ya existe nota registrada, no se puede modificar"
                        );
                    } else {
                        if ($nota >= 0 && $nota <= 20) {
                            $model = new Nota();
                            $model->curso_id = $curso_id;
                            $model->alumno_id = $alumno->alumno_id;
                            $model->nota = $nota;
                            $model->estado = ($nota > 11 ? 'APROBADO' : 'DESAPROBADO');
                            if ($model->save()) {
                                $return = array(
                                    "estado" => 1, "mensaje" => "OK"
                                );
                            } else {
                                $return = array(
                                    "estado" => 0, "mensaje" => "NO SE PUDO REGISTRAR NOTA"
                                );
                            }
                        } else {
                            $return = array(
                                "estado" => 0, "mensaje" => "NOTA INVALIDA"
                            );
                        }
                    }
                } else {
                    $return = array(
                        "estado" => 0, "mensaje" => "NO EXISTE EL CURSO INGRESADO"
                    );
                }
            } else {
                $return = array(
                    "estado" => 0, "mensaje" => "NO EXISTE EL ALUMNO INGRESADO"
                );
            }
        } catch (\Throwable $th) {
            $return = array(
                "estado" => 0, "mensaje" => "ERROR INESPERADO"
            );
        } finally {
            echo json_encode($return);
        }
    }
}
